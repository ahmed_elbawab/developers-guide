import React, { Component } from "react";

import "./css/navBarComponent.css";

class NavBar extends Component {
  state = {};
  render() {
    return (
      <header id="header">
        <div class="container">
          <nav class="navbar navbar-expand-lg navbar-custom bg-custom rounded">
            <div id="header-logo">
              <a class="navbar-brand" href="#home">
                Developers' Guide <span>FROM GI</span>
              </a>
            </div>

            <div class="navbar-header">
              <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarsExample01"
                aria-controls="navbarsExample01"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span class="navbar-toggler-icon" />
              </button>
            </div>

            <div class="collapse navbar-collapse" id="navbar-list">
              <ul class="navbar-nav mr-auto nav navbar-nav navbar-right">
                <li class="nav-item">
                  <a class="nav-link" href="#home">
                    Home
                  </a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#home">
                    About
                  </a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#footer">
                    Contact Us
                  </a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="#home">
                    النسخة العربية
                  </a>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </header>
    );
  }
}

export default NavBar;
