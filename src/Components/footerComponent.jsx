import React, { Component } from "react";

import "./css/footerComponent.css";

class Footer extends Component {
  state = {};
  render() {
    return (
      <footer class="footer-area section-gap" id="footer">
        <link
          rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ"
          crossorigin="anonymous"
        />

        <div class="container">
          <div id="footer-header">
            <h1>We are happy to hear From you.</h1>
            <i class="fas fa-headphones" />
          </div>

          <div class="row">
            <div
              class="col-lg-12 col-sm-12 footer-social text-center"
              id="footer-social"
            >
              <a href="#home">
                <i class="fab fa-facebook-f" />
              </a>
              <a href="#home">
                <i class="fas fa-phone" />
              </a>
              <a href="#home">
                <i class="fab fa-youtube" />
              </a>
              <a
                href="https://mail.google.com/mail/?view=cm&fs=1&to=getit.all.startup@gmail.com"
                target="blank"
              >
                <i class="fab fa-google" />
              </a>
            </div>
          </div>
        </div>

        <div id="copy-right">
          <p>
            Copyright &copy;2018 All rights reserved | This web site is made
            with
            <i class="fas fa-heart" />
            by
            <a href="#banner-area" target="_blank">
              GET IT
            </a>
          </p>
        </div>
      </footer>
    );
  }
}

export default Footer;
