import React, { Component } from "react";

import "./css/bodyComponent.css";

import Topic from "./topicComponent";

class Body extends Component {
  state = {
    Data: [
      {
        topic_title: "Desktop Languages",
        items: ["Java", "C++", "C", "Python", "C#", "Kotlin"]
      },
      {
        topic_title: "Front End Web Languages",
        items: [
          "HTML",
          "CSS",
          "HTML 5",
          "CSS 3",
          "Bootstrap",
          "Javascript",
          "React",
          "Angular"
        ]
      },
      {
        topic_title: "Back End Web Languages",
        items: ["PHP", "Ruby", "Node.JS", "Python", "Java", "Laravel"]
      },
      {
        topic_title: "Algorithms",
        items: [
          "Sort",
          "Search",
          "Greedy",
          "DP",
          "Graphs",
          "Geometry",
          "Brackting",
          "Strings"
        ]
      },
      {
        topic_title: "Data Structure",
        items: [
          "Linked List",
          "Queues",
          "Stack",
          "Arrays",
          "Trees",
          "B-Trees",
          "Red Black Trees"
        ]
      },
      {
        topic_title: "Hardware",
        items: ["Arduino", "VHDL", "C"]
      },
      {
        topic_title: "Design",
        items: ["Photo Shop", "Illustrator", "3D Max", "Maya", "Cinema 4D"]
      },
      {
        topic_title: "Vedio Editing",
        items: ["Premier", "After Effect"]
      }
    ]
  };
  render() {
    return (
      <div className="body">
        {this.state.Data.map(item => (
          <Topic topic_title={item.topic_title} items={item.items} />
        ))}
      </div>
    );
  }
}

export default Body;
